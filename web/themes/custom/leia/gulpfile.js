const gulp = require('gulp');
const postcss = require('gulp-postcss')
const browserSync = require("browser-sync").create();

const paths = {
  dist: "dist/",
  styles: {
    src: 'src/css/*.css',
    dest: 'dist/css'
  },
  images: {
    src: "src/images/**/*.*",
    dest: "dist/images/"
  },
  js: {
    src: "src/js/**/*.js",
    dest: "dist/js/"
  },
  html: {
    src: "src/*.html",
    dist: "dist/"
  },
  font: {
    src: "src/font/**/*.*",
    dest: "dist/font/"
  },
  components: {
    src: "src/css/components/**/*.css",
    dest: "dist/css/components",
  }
};

// Static server
gulp.task('browser-sync', function() {
  browserSync.init({
      server: {
        baseDir: paths.dist
      },
      port: 5000,
  });
});

// Styles
gulp.task('appStyle', (done) => {
  gulp
    .src(paths.styles.src)
    .pipe(postcss([
      require('tailwindcss'),
      require('autoprefixer'),
      require('@tailwindcss/forms'),
      require('@tailwindcss/typography')
    ]))
    .pipe(gulp.dest(paths.styles.dest));
  done();
})

gulp.task('components', (done) => {
  gulp
    .src(paths.components.src)
    .pipe(postcss([
      require('tailwindcss'),
      require('autoprefixer'),
      require('@tailwindcss/forms'),
      require('@tailwindcss/typography')
    ]))
    .pipe(gulp.dest(paths.components.dest));
  done();
})

// Images
gulp.task('images', (done) => {
  gulp
    .src(paths.images.src)
    .pipe(gulp.dest(paths.images.dest));
  done();
})

// Javascript
gulp.task('js', (done) => {
  gulp
    .src(paths.js.src)
    // .pipe(uglify())
    .pipe(gulp.dest(paths.js.dest));
  done();
})

// Font
gulp.task('font', (done) => {
  gulp
    .src(paths.font.src)
    .pipe(gulp.dest(paths.font.dest));
  done();
})

gulp.task('demoHTML', (done) => {
  gulp
    .src(`${paths.html.src}`)
    .pipe(gulp.dest(paths.html.dist));
  done();
})

gulp.task('build', gulp.series('demoHTML', 'font', 'js', 'images', 'appStyle', 'components'));
gulp.task('default', gulp.series('build'));

gulp.task('watch', function () {
  gulp.watch(paths.styles.src, gulp.task('appStyle'))
  gulp.watch(paths.components.src, gulp.task('components'))
  gulp.watch(paths.images.src, gulp.task('images'))
  gulp.watch(paths.js.src, gulp.task('js'))
});
