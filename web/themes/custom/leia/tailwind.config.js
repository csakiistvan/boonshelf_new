const { spacing, fontFamily } = require('tailwindcss/defaultTheme')

module.exports = {
  content: [
    './templates/**/*.twig',
    './src/includes/**/*.inc',
  ],
  theme: {
    container: {
      center: true,
    },
    colors: {
      gray: {
        25: '#F8F9FA',
        50: '#F6F7F9',
        100: '#EDEEF1',
        200: '#D6DAE1',
        300: '#B2BAC7',
        400: '#8895A8',
        500: '#68778E',
        600: '#546175',
        700: '#454F5F',
        800: '#3C4350',
        900: '#373D48',
        950: '#23272E'
      },
      coral: {
        50: '#FAF5F6',
        100: '#F6EDEF',
        200: '#EFDBDF',
        300: '#E2BFC7',
        400: '#CF97A2',
        500: '#BE7683',
        600: '#AB5F68',
        700: '#8F474E',
        800: '#773D41',
        900: '#65363A',
        950: '#3B1C1E'
      },
      black: '#000',
      current: 'currentcolor',
      transparent: 'transparent',
      white: '#fff'
    },
    fontFamily: {
      heading: ['Lexend', ...fontFamily.sans]
    },
    extend: {
      backgroundImage: {
        'icon-globe-light': `url('data:image/svg+xml,<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"><g clip-path="url(%23clip0_561_12153)"><path d="M12 21C16.9706 21 21 16.9706 21 12C21 7.02944 16.9706 3 12 3C7.02944 3 3 7.02944 3 12C3 16.9706 7.02944 21 12 21Z" stroke="%23546175" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/><path d="M4.65002 17.1938L5.7469 16.5281C5.85568 16.461 5.94551 16.3673 6.00786 16.2557C6.07021 16.1441 6.10301 16.0185 6.10315 15.8906L6.1219 12.5063C6.12339 12.366 6.16575 12.2291 6.24377 12.1125L8.10002 9.1969C8.15573 9.11076 8.2283 9.03679 8.31338 8.97947C8.39845 8.92215 8.49426 8.88266 8.59501 8.86339C8.69577 8.84411 8.79938 8.84545 8.89961 8.86733C8.99983 8.8892 9.09459 8.93115 9.17815 8.99065L11.0156 10.3219C11.1742 10.4324 11.3672 10.4823 11.5594 10.4625L14.5125 10.0594C14.6917 10.0347 14.8553 9.94455 14.9719 9.80627L17.0531 7.40627C17.1766 7.25998 17.2402 7.07247 17.2313 6.88127L17.1281 4.60315" stroke="%23546175" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/><path d="M17.5407 19.0969L16.5282 18.0844C16.4345 17.9908 16.3183 17.9229 16.1907 17.8875L14.1751 17.3625C13.998 17.314 13.8449 17.2023 13.7447 17.0486C13.6445 16.8948 13.6041 16.7096 13.6313 16.5281L13.8469 15.0094C13.8683 14.8816 13.9217 14.7614 14.002 14.6597C14.0823 14.5581 14.187 14.4784 14.3063 14.4281L17.1563 13.2375C17.2887 13.1823 17.4341 13.1662 17.5753 13.1911C17.7166 13.216 17.8477 13.2809 17.9532 13.3781L20.2876 15.5156" stroke="%23546175" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></g><defs><clipPath id="clip0_561_12153"><rect width="24" height="24" fill="white"/></clipPath></defs></svg>')`,
        'icon-globe-dark': `url('data:image/svg+xml,<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"><g clip-path="url(%23clip0_561_12153)"><path d="M12 21C16.9706 21 21 16.9706 21 12C21 7.02944 16.9706 3 12 3C7.02944 3 3 7.02944 3 12C3 16.9706 7.02944 21 12 21Z" stroke="%23D6DAE1" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/><path d="M4.65002 17.1938L5.7469 16.5281C5.85568 16.461 5.94551 16.3673 6.00786 16.2557C6.07021 16.1441 6.10301 16.0185 6.10315 15.8906L6.1219 12.5063C6.12339 12.366 6.16575 12.2291 6.24377 12.1125L8.10002 9.1969C8.15573 9.11076 8.2283 9.03679 8.31338 8.97947C8.39845 8.92215 8.49426 8.88266 8.59501 8.86339C8.69577 8.84411 8.79938 8.84545 8.89961 8.86733C8.99983 8.8892 9.09459 8.93115 9.17815 8.99065L11.0156 10.3219C11.1742 10.4324 11.3672 10.4823 11.5594 10.4625L14.5125 10.0594C14.6917 10.0347 14.8553 9.94455 14.9719 9.80627L17.0531 7.40627C17.1766 7.25998 17.2402 7.07247 17.2313 6.88127L17.1281 4.60315" stroke="%23D6DAE1" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/><path d="M17.5407 19.0969L16.5282 18.0844C16.4345 17.9908 16.3183 17.9229 16.1907 17.8875L14.1751 17.3625C13.998 17.314 13.8449 17.2023 13.7447 17.0486C13.6445 16.8948 13.6041 16.7096 13.6313 16.5281L13.8469 15.0094C13.8683 14.8816 13.9217 14.7614 14.002 14.6597C14.0823 14.5581 14.187 14.4784 14.3063 14.4281L17.1563 13.2375C17.2887 13.1823 17.4341 13.1662 17.5753 13.1911C17.7166 13.216 17.8477 13.2809 17.9532 13.3781L20.2876 15.5156" stroke="%23D6DAE1" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></g><defs><clipPath id="clip0_561_12153"><rect width="24" height="24" fill="white"/></clipPath></defs></svg>')`,
        'icon-heart-light': `url('data:image/svg+xml,<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"><g clip-path="url(%23clip0_727_52500)"><path d="M12 20.25C12 20.25 2.625 15 2.625 8.62501C2.625 7.49803 3.01546 6.40585 3.72996 5.53431C4.44445 4.66277 5.43884 4.0657 6.54393 3.84468C7.64903 3.62366 8.79657 3.79235 9.79131 4.32204C10.7861 4.85174 11.5665 5.70972 12 6.75001C12.4335 5.70972 13.2139 4.85174 14.2087 4.32204C15.2034 3.79235 16.351 3.62366 17.4561 3.84468C18.5612 4.0657 19.5555 4.66277 20.27 5.53431C20.9845 6.40585 21.375 7.49803 21.375 8.62501C21.375 15 12 20.25 12 20.25Z" stroke="%23AB5F68" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></g><defs><clipPath id="clip0_727_52500"><rect width="24" height="24" fill="white"/></clipPath></defs></svg>')`,
        'icon-heart-full': `url('data:image/svg+xml,<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"><g clip-path="url(%23clip0_727_52544)"><path d="M12 20.25C12 20.25 2.625 15 2.625 8.62501C2.625 7.49803 3.01546 6.40585 3.72996 5.53431C4.44445 4.66277 5.43884 4.0657 6.54393 3.84468C7.64903 3.62366 8.79657 3.79235 9.79131 4.32204C10.7861 4.85174 11.5665 5.70972 12 6.75001C12.4335 5.70972 13.2139 4.85174 14.2087 4.32204C15.2034 3.79235 16.351 3.62366 17.4561 3.84468C18.5612 4.0657 19.5555 4.66277 20.27 5.53431C20.9845 6.40585 21.375 7.49803 21.375 8.62501C21.375 15 12 20.25 12 20.25Z" fill="%23AB5F68" stroke="%23AB5F68" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></g><defs><clipPath id="clip0_727_52544"><rect width="24" height="24" fill="white"/></clipPath></defs></svg>')`,
      },
      maxWidth: {
        'container': '76rem',
      },
      fontWeight: {
        'semilight': '350',
        'semimedium': '550'
      },
      fontSize: {
        'title': '2.5rem',
        '15lg': '1.5rem',
        '35xl': '2rem',
      },
      borderRadius: {
        DEFAULT: '.5rem',
        small: '.25rem',
        large: '1rem',
      },
    }
  },
  variants: {
    extend: {},
  },
  plugins: [
    require('@tailwindcss/forms'),
    require('@tailwindcss/typography'),
  ],
};
